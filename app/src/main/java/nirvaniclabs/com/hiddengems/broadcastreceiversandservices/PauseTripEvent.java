package nirvaniclabs.com.hiddengems.broadcastreceiversandservices;

public class PauseTripEvent {
    public final boolean pause;

    public PauseTripEvent(boolean pause) {
        this.pause = pause;
    }
}
