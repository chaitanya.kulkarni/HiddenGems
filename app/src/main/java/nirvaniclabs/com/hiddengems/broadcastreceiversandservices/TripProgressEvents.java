package nirvaniclabs.com.hiddengems.broadcastreceiversandservices;

/**
 * Created by Amrut on 12 december
 */
public class TripProgressEvents {

    public final float mTripProgress;
    public final String mTripName;

    public TripProgressEvents(float mTripProgress, String mTripName) {
        this.mTripProgress = mTripProgress;
        this.mTripName = mTripName;
    }
}
