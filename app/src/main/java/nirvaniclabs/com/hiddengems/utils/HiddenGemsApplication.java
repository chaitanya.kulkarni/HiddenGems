package nirvaniclabs.com.hiddengems.utils;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.multidex.MultiDexApplication;

import com.crashlytics.android.Crashlytics;
import com.squareup.leakcanary.LeakCanary;

import io.fabric.sdk.android.Fabric;

/**
 * Created by Amrut on 10/26/15.
 * This class contains all the resources required in the entire application.
 */
public class HiddenGemsApplication extends MultiDexApplication {

    public SharedPreferences mGlobalSharedPreferences;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this,new Crashlytics());
        LeakCanary.install(this);
        mGlobalSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
    }
}
