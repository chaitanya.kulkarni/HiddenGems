package nirvaniclabs.com.hiddengems.utils;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

/**
 * Created by Amrut on 10/26/15.
 * This will contain all the Constants and keys used in the entire application.
 */
public class Constants {
    public static final String PREF_USER_EMAIL = "UserEmail";

    public static final String PREF_MAP_LOADED = "MapLoaded";

    public static final String PREF_VERSION_CODEKEY = "VersionCcode";

    public static final String ARGUMENT_ACTIONBAR_TITLE = "ActionBarTitle";

    public static final String PREF_VERSION_CODE_KEY = "version_code";

    public static final String PREFS_IS_MAP_LOADED_ON_Source_SCREEN = "IS_MAP_LOADED_ON_SOURCE_SCREEN";

    public static final String PREFS_IS_MAP_LOADED_ON_DEST_SCREEN = "IS_MAP_LOADED_ON_DEST_SCREEN";

    public static final String PREFS_IS_MAP_LOADED_ON_TRIP_SCREEN = "IS_MAP_LOADED_ON_TRIP_SCREEN";

    public static final double CITY_POST_PUNE_LAT = 18.515699;

    public static final double CITY_POST_PUNE_LNG = 73.856797;

    public static final float DEFAULT_RADIUS_PUNE = 50; // IN KM

    public static final double DEFAULT_ERROR_FOR_TRIP = 0.05; // IN KM

    public static final long DEFAULT_DELAY_BETWEEN_CALLS = 10000; // 10 seconds

    public static final float DEFAULT_DISTANCE_BETWEEN_CURRENT_LOCATION_AND_SOURCE = 50; // In Meters

    public static final float DEFAULT_TRIP_DISTANCE = 1; // IN KM

    public static final int LOCATION_UPDATE_INTERVAL = 10000;

    public static final int LOCATION_UPDATE_FASTEST_INTERVAL = 5000;

    public static final LatLngBounds BOUNDS_PUNE_CITY = new LatLngBounds(
            new LatLng(18.413478, 73.739478), new LatLng(18.635755, 73.986457));

    public static boolean mIsFreeRoamMode;

    public static boolean mIsExploreMode;

    public static boolean mIsNormalTrip;
}
