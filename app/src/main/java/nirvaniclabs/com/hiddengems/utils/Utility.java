package nirvaniclabs.com.hiddengems.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import nirvaniclabs.com.hiddengems.R;
import nirvaniclabs.com.hiddengems.customwidgets.CustomAutoCompleteTextView;

/**
 * Created by Amrut on 10/26/15.
 * This class contains all the utility methods needed in the entire application.
 */
public class Utility {
    private static Utility _instance = null;
    private Context mContext = null;
    private Typeface mTypeface = null;//, mTypefaceBold = null;

    private Utility(Context context) {
        mContext = context;
    }

    /**
     * Singleton class to instantiate the class variables.
     */
    public static Utility get_instance(Context context) {
        if (null == _instance)
            _instance = new Utility(context);
        return _instance;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return _instance;
    }

    /**
     * Returns font type used by the applicaion.
     *
     * @return regular type face
     */
    public Typeface getFontTypeface() {
        if (mTypeface == null) {
            mTypeface = Typeface.createFromAsset(mContext.getAssets(), "font/" + mContext.getResources().getString(R.string.type_face_regular));
        }
        return mTypeface;
    }

    /**
     * Returns font type used by the applicaion.
     *
     * @return bold type face
     */
//    public Typeface getBoldFontTypeface() {
//        if (mTypefaceBold == null) {
//            mTypefaceBold = Typeface.createFromAsset(mContext.getAssets(), "font/" + mContext.getResources().getString(R.string.type_face_bold));
//        }
//        return mTypefaceBold;
//    }

    /**
     * This function is used to check the email validation.
     *
     * @param email : email string
     * @return true if email id is valid else return false.
     */
    public static boolean isEmailValid(String email) {
        String regExpn = "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]|[\\w-]{2,}))@" + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?" + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\." + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?" + "[0-9]{1,2}|25[0-5]|2[0-4][0-9]))|" + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

        Pattern mPattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
        Matcher mMatcher = mPattern.matcher(email);
        return mMatcher.matches();
    }

    public static void makeInstanceNull() {
        _instance = null;
    }

    /**
     * Hide the virtual keypad.
     */
    public static void hideKeypad(EditText editText, Context context) {
        if (editText != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(editText.getWindowToken(), 0);
        }
    }

    public static void hideKeypadForAutomCompleteView(CustomAutoCompleteTextView customAutoCompleteTextView, Context context) {
        if (customAutoCompleteTextView != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(customAutoCompleteTextView.getWindowToken(), 0);
        }
    }

    public static void adjustSoftInput(Activity context) {
        (context).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
    }

    /**
     * Show the virtual keypad.
     */
//    public static void showKeypad(EditText editText, Context context) {
//        if (editText != null) {
//            InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
//            inputMethodManager.showSoftInput(editText, 0, null);
//        }
//    }


    float scale = 0;

    /**
     * @return DIP value.
     */
    public int getDipValue(int value, Context context) {
        if (scale == 0)
            scale = context.getResources().getDisplayMetrics().density;

        return (int) (value * scale + 0.5f);
    }

    /**
     * Fetches screen orientation
     */
//    @SuppressWarnings("deprecation")
//    public static int getScreenOrientation(Activity activity) {
//        try {
//            Display display = activity.getWindowManager().getDefaultDisplay();
//            int orientation;
//            if (display.getWidth() < display.getHeight()) {
//                orientation = Configuration.ORIENTATION_PORTRAIT;
//            } else {
//                orientation = Configuration.ORIENTATION_LANDSCAPE;
//            }
//            return orientation;
//        } catch (Exception | Error e) {
//            e.printStackTrace();
//        }
//        return 0;
//    }

    /**
     * returns the orientation for a CONTEXT that is independent of any activity
     */

//    @SuppressWarnings("deprecation")
//    public static int getScreenOrientation(Context context) {
//        try {
//            WindowManager mWindowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
//            Display display = mWindowManager.getDefaultDisplay();
//            int orientation;
//            if (display.getWidth() < display.getHeight()) {
//                orientation = Configuration.ORIENTATION_PORTRAIT;
//            } else {
//                orientation = Configuration.ORIENTATION_LANDSCAPE;
//            }
//            return orientation;
//        } catch (Exception | Error e) {
//            e.printStackTrace();
//        }
//        return 0;
//    }

    /**
     * Checks whether device has a large screen or small screen.
     *
     * @return true for large screen device
     */
    public static boolean isLargeScreen(Context context) {
        return context.getResources().getBoolean(R.bool.isTablet);
    }

    /**
     * Returns application's current version.
     *
     * @return String
     */
//    public static String getApplicationCurrentVersionName(Activity activity) {
//        PackageInfo packageInfo;
//        String versionName = "1.0"; // default
//        try {
//            packageInfo = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0);
//            versionName = packageInfo.versionName;
//            int vCode = packageInfo.versionCode;
//            System.out.println("Utility> VersionName: " + versionName + " | VersionCode: " + vCode);
//        } catch (Exception | Error e) {
//            e.printStackTrace();
//        }
//        return versionName;
//    }
//
//    public static float convertPixelsToDp(float px, Context context) {
//        Resources resources = context.getResources();
//        DisplayMetrics metrics = resources.getDisplayMetrics();
//        return px / (metrics.densityDpi / 160f);
//    }

    /**
     * Parse the color string, and return the corresponding color-int. Returns
     * '0x00000000' if any exception occurs.
     *
     * @param colorString <br>
     *                    e.g. #FFFF00FF
     * @return e.g. 0x00000000
     */
//    public static int getColorCode(String colorString) {
//        try {
//            if (!TextUtils.isEmpty(colorString) && colorString.charAt(0) == '#' && colorString.substring(1, colorString.length()).length() == 8) {
//                return Color.parseColor(colorString);
//            } else
//                return 0xFF000000;
//        } catch (Exception e) {
//            e.printStackTrace();
//            return 0x00000000;
//        }
//    }

    /**
     * @return capitalize string
     * <p/>
     * This method capitalize the first letter of each word in the
     * string inputString.
     */
//    public static String capitalizeFirstLetter(String inputString) {
//        String outputString = "";
//        try {
//            if (!TextUtils.isEmpty(inputString)) {
//                if (inputString.length() > 1) {
//                    String[] arr = inputString.split(" ");
//                    StringBuffer buffer = new StringBuffer();
//
//                    for (String anArr : arr) {
//                        buffer.append(Character.toUpperCase(anArr.charAt(0)));
//                        buffer.append(anArr.substring(1).toLowerCase(Locale.getDefault())).append(" ");
//                    }
//                    outputString = buffer.toString().trim();
//                } else if (inputString.length() == 1) {
//                    outputString = inputString.substring(0, 1).toUpperCase();
//                }
//            }
//            return outputString;
//        } catch (Exception e) {
//            e.printStackTrace();
//            return outputString;
//        }
//    }

    /**
     * Checks if network connection is available or not.
     *
     * @return true if network available
     */
    public boolean isNetworkConnected(Activity activity) {
        ConnectivityManager connectivityManager = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        return networkInfo != null && networkInfo.isConnected();
    }


    public void isInternetConnected(NetworkCheckerInterFace networkCheckerInterFace) {
        new NetworkCheckerTask(networkCheckerInterFace).execute();
    }

    class NetworkCheckerTask extends AsyncTask<Void, Void, Boolean> {

        private NetworkCheckerInterFace networkCheckerInterFace;

        public NetworkCheckerTask(NetworkCheckerInterFace networkCheckerInterFace) {
            this.networkCheckerInterFace = networkCheckerInterFace;
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            return hasActiveInternetConnection(mContext);
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if (this.networkCheckerInterFace != null)
                this.networkCheckerInterFace.setIsNetworkAvailable(aBoolean);
        }
    }

    public interface NetworkCheckerInterFace {
        void setIsNetworkAvailable(boolean isNetworkAvailable);
    }

    private boolean hasActiveInternetConnection(Context context) {
        if (Utility.get_instance(context).isNetworkConnected((Activity) context)) {
            try {
                HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
                urlc.setRequestProperty("User-Agent", "Test");
                urlc.setRequestProperty("Connection", "close");
                urlc.setConnectTimeout(1500);
                urlc.connect();
                return (urlc.getResponseCode() == 200);
            } catch (IOException e) {
                Log.e("Utility", "Error checking internet connection", e);
            }
        } else {
            Log.d("Utility", "No network available!");
        }
        return false;
    }

    /**
     * This method check if we are able to use google play services or not
     */
    public boolean googleServiceIsOK(Activity activity) {
        int isServiceOk = GooglePlayServicesUtil.isGooglePlayServicesAvailable(activity);

        if (isServiceOk == ConnectionResult.SUCCESS) {
            return true;
        } else if (GooglePlayServicesUtil.isUserRecoverableError(isServiceOk)) {
            int GPS_ERRORDIALOG_REQUEST = 1001;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(isServiceOk, activity, GPS_ERRORDIALOG_REQUEST);
            dialog.show();
            return false;
        } else {
            Toast.makeText(activity, R.string.google_play_service_error, Toast.LENGTH_LONG).show();
            return false;
        }
    }


    /**
     * Calculates distance between source and destination LatLng
     *
     * @param source      latitude
     * @param destination latitude
     * @return distance in KM
     */
    public float distanceBetween(LatLng source, LatLng destination) {

        if (source == null || destination == null)
            return 0;

//        Location location = new Location("Source");
//        location.setLatitude(source.latitude);
//        location.setLongitude(source.longitude);
//
//
//        Location location1 = new Location("Destination");
//        location1.setLatitude(destination.latitude);
//        location1.setLongitude(destination.longitude);
//
//        float v = location.distanceTo(location1);
//                .setLatitude(source.latitude)


        float[] results = new float[3];
        Location.distanceBetween(source.latitude, source.longitude, destination.latitude, destination.longitude, results);
        // Convert meters to KM
        float v = results[0] / 1000;

        BigDecimal bd = new BigDecimal(Float.toString(v));
        bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);

        return bd.floatValue();
    }

//    private void displayImagePickerDialog(final Context context) {
//        final int PICK_FROM_CAMERA = 1001;
//        final int PICK_FROM_FILE = 2001;
//        final Uri[] mImageCaptureUri = new Uri[1];
//        final String[] items = new String[]{"From Camera", "From SD Card"};
//        ArrayAdapter<String> adapter = new ArrayAdapter<>(context, android.R.layout.select_dialog_item, items);
//        AlertDialog.Builder builder = new AlertDialog.Builder(context);
//
//        builder.setTitle("Select Image");
//
//        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int item) {
//                if (item == 0) {
//                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                    File file = new File(Environment.getExternalStorageDirectory(),
//                            "tmp_avatar.jpg");
//                    mImageCaptureUri[0] = Uri.fromFile(file);
//
//                    try {
//                        intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri[0]);
//                        intent.putExtra("return-data", true);
//
//                        ((Activity) context).startActivityForResult(intent, PICK_FROM_CAMERA);
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//
//                    dialog.cancel();
//                } else {
//                    Intent intent = new Intent();
//
//                    intent.setType("image/*");
//                    intent.setAction(Intent.ACTION_GET_CONTENT);
//
//                    ((Activity) context).startActivityForResult(Intent.createChooser(intent, "Complete action using"), PICK_FROM_FILE);
//                }
//            }
//        });
//        AlertDialog dialog = builder.create();
//        dialog.show();
//    }
}
