package nirvaniclabs.com.hiddengems.adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import nirvaniclabs.com.hiddengems.R;
import nirvaniclabs.com.hiddengems.activities.HomeActivity;
import nirvaniclabs.com.hiddengems.fragments.PickupSourceFragment;
import nirvaniclabs.com.hiddengems.models.Flower;
import nirvaniclabs.com.hiddengems.utils.Utility;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CustomViewHolder> {
    private ArrayList<Flower> mFlowerList;
    Context mContext;
    String url = "http://services.hanselandpetal.com/photos/";

    public CategoryAdapter(ArrayList<Flower> flowers, Context context) {
        mContext = context;
        mFlowerList = flowers;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_flower_view, null);

        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder holder, int position) {
        Flower flower = mFlowerList.get(position);

        holder.imageView.setOnClickListener(onClickListener);
        holder.imageView.setTag(holder);

        Picasso.with(mContext)
                .load(url + flower.getPhoto())
                .resize(Utility.get_instance(mContext).getDipValue(160, mContext), Utility.get_instance(mContext).getDipValue(120, mContext))
        .noPlaceholder()
                .error(R.drawable.progress_animation)
                .centerCrop()
                .into(holder.imageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        holder.mProgressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        holder.mProgressBar.setVisibility(View.GONE);
                    }
                });
    }

    @Override
    public int getItemCount() {
        return (mFlowerList != null) ? mFlowerList.size() : 0;
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {
        protected ImageView imageView;
        protected ProgressBar mProgressBar;

        public CustomViewHolder(View itemView) {
            super(itemView);
            this.imageView = (ImageView) itemView.findViewById(R.id.img_view);
            this.mProgressBar = (ProgressBar) itemView.findViewById(R.id.progress_bar_placeholder);
        }
    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            CustomViewHolder customViewHolder = (CustomViewHolder) v.getTag();
            int position = customViewHolder.getAdapterPosition();

            if (position != RecyclerView.NO_POSITION) {
                Flower flower = mFlowerList.get(position);
                Toast.makeText(mContext, flower.getName(), Toast.LENGTH_SHORT).show();
                ((HomeActivity) mContext).replaceFragment(new PickupSourceFragment(), mContext.getString(R.string.explore), PickupSourceFragment.TAG);
            }
        }
    };
}
