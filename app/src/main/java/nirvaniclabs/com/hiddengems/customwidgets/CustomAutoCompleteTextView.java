package nirvaniclabs.com.hiddengems.customwidgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.AutoCompleteTextView;

import nirvaniclabs.com.hiddengems.R;

/**
 * Created by amrut on 10/27/15.
 * Custom Auto complete text view
 */
public class CustomAutoCompleteTextView extends AutoCompleteTextView {
    public CustomAutoCompleteTextView(Context context) {
        super(context);
        init(null);
    }

    public CustomAutoCompleteTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init(attributeSet);
    }

    public CustomAutoCompleteTextView(Context context, AttributeSet attributeSet, int defStyle) {
        super(context, attributeSet, defStyle);
        init(attributeSet);
    }

    private void init(AttributeSet attrs) {
        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.CustomAutoCompleteTextView);
            String fontName = a.getString(R.styleable.CustomAutoCompleteTextView_FontName);
            if (fontName != null) {
                Typeface myTypeface = Typeface.createFromAsset(getContext().getAssets(), "font/" + fontName);
                setTypeface(myTypeface);
            }
            a.recycle();
        }
    }

    /**
     * Customizing AutoCompleteTextView to return Place Description
     * corresponding to the selected item
     */
//    @Override
//    protected CharSequence convertSelectionToString(Object selectedItem) {
//        HashMap<String, String> hashMap = (HashMap<String, String>) selectedItem;
//        return hashMap.get("description");
//    }
}
