package nirvaniclabs.com.hiddengems.network;


import java.util.List;

import nirvaniclabs.com.hiddengems.models.Flower;
import retrofit.Call;
import retrofit.http.GET;

public interface HiddenGemsService {

    @GET("feeds/flowers.json")
    Call<List<Flower>> getData();
}
