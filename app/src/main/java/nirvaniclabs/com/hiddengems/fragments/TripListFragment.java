package nirvaniclabs.com.hiddengems.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import nirvaniclabs.com.hiddengems.R;
import nirvaniclabs.com.hiddengems.activities.HomeActivity;

/**
 * A simple {@link Fragment} subclass.
 * This class will display all the trips for a specified category.
 * User will get all the details of trip from here.
 * This will contain an view pager.
 */
public class TripListFragment extends Fragment {

    private AppCompatActivity mActivity;
    public static String TAG = "TripListFragment";

    public TripListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof AppCompatActivity)
            mActivity = (AppCompatActivity) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            ActionBar actionBar = mActivity.getSupportActionBar();
            if (actionBar != null) {
                actionBar.setDisplayShowTitleEnabled(false);
                actionBar.setDisplayHomeAsUpEnabled(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        try {
            ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.fragment_trip_list, container, false);
            return viewGroup;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        ((HomeActivity) mActivity).setDrawerIndicatorEnabled(false);
        ((HomeActivity) mActivity).enableDisableDrawer(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    @Override
    public void onStop() {
        ((HomeActivity) mActivity).enableDisableDrawer(DrawerLayout.LOCK_MODE_UNLOCKED);
        super.onStop();
    }
}
