package nirvaniclabs.com.hiddengems.fragments;


import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import nirvaniclabs.com.hiddengems.R;
import nirvaniclabs.com.hiddengems.customwidgets.CustomButton;
import nirvaniclabs.com.hiddengems.customwidgets.CustomEditText;
import nirvaniclabs.com.hiddengems.utils.Utility;

/**
 * This fragment handles the required things if user forgot his/her password
 */
public class ForgotPasswordFragment extends Fragment {

    public static String TAG = "ForgotPasswordFragment";
    private AppCompatActivity mActivity;
    private CustomEditText mEditTextEmail;
    private CustomButton mButtonSend;

    public ForgotPasswordFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof AppCompatActivity)
            mActivity = (AppCompatActivity) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            Utility.adjustSoftInput(mActivity);
            ActionBar actionBar = mActivity.getSupportActionBar();
            if (actionBar != null) {
                actionBar.setDisplayShowTitleEnabled(false);
                actionBar.setDisplayHomeAsUpEnabled(true);
                actionBar.setBackgroundDrawable(new ColorDrawable(0xFFFFFF00));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        try {

            ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.fragment_forgot_password, container, false);

            initViews(viewGroup);

            mButtonSend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (TextUtils.isEmpty(mEditTextEmail.getText().toString())) {
                        mEditTextEmail.setError(mActivity.getString(R.string.email_empty));
                        return;
                    }

                    if (!Utility.isEmailValid(mEditTextEmail.getText().toString().trim())) {
                        mEditTextEmail.setError(mActivity.getString(R.string.email_invalid));
                        return;
                    }

                    Utility.hideKeypad(mEditTextEmail, mActivity);

                    if (!Utility.get_instance(mActivity).isNetworkConnected(mActivity)) {
                        Toast.makeText(mActivity, mActivity.getString(R.string.no_internet_message), Toast.LENGTH_LONG).show();
                        return;
                    }

                    // TODO: send request to server and if success redirect user to login screen

                    mActivity.onBackPressed();

                }
            });
            return viewGroup;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private void initViews(ViewGroup viewGroup) {
        mEditTextEmail = (CustomEditText) viewGroup.findViewById(R.id.edittext_email);
        mButtonSend = (CustomButton) viewGroup.findViewById(R.id.button_send);
    }

    @Override
    public void onPause() {
        super.onPause();
        Utility.hideKeypad(mEditTextEmail, mActivity);
    }
}
