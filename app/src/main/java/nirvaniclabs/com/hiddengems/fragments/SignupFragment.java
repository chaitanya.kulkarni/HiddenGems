package nirvaniclabs.com.hiddengems.fragments;


import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import nirvaniclabs.com.hiddengems.R;
import nirvaniclabs.com.hiddengems.activities.HomeActivity;
import nirvaniclabs.com.hiddengems.customwidgets.CustomButton;
import nirvaniclabs.com.hiddengems.customwidgets.CustomEditText;
import nirvaniclabs.com.hiddengems.utils.ImagePicker;
import nirvaniclabs.com.hiddengems.utils.Utility;

/**
 * This fragment will handle user sign-up process.
 * On successful sign-up user will be logged into application.
 */
public class SignupFragment extends Fragment {

    private static final int PICK_IMAGE_ID = 1234;
    public static String TAG = "SignupFragment";

    private AppCompatActivity mActivity;

    private CustomEditText mEditTextFullName, mEditTextEmail, mEditTextPassWord, mEditTextConfirmPassWord;
    private CustomButton mButtonRegister;
    private ImageView mImageView;

    public SignupFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof AppCompatActivity)
            mActivity = (AppCompatActivity) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            ActionBar actionBar = mActivity.getSupportActionBar();
            if (actionBar != null) {
                actionBar.setDisplayShowTitleEnabled(false);
                actionBar.setDisplayHomeAsUpEnabled(true);
                actionBar.setBackgroundDrawable(new ColorDrawable(0xFFFFFF00));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        try {
            ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.fragment_signup, container, false);
            initViews(viewGroup);

            mImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent chooseImageIntent = ImagePicker.getPickImageIntent(mActivity);
                    startActivityForResult(chooseImageIntent, PICK_IMAGE_ID);
                }
            });
            mButtonRegister.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (TextUtils.isEmpty(mEditTextFullName.getText().toString())) {
                        mEditTextFullName.setError(mActivity.getString(R.string.registration_name_empty));
                        return;
                    }

                    if (mEditTextFullName.getText().toString().length() < 5) {
                        mEditTextFullName.setError(mActivity.getString(R.string.registration_name_invalid));
                        return;
                    }

                    Utility.hideKeypad(mEditTextFullName, mActivity);

                    if (TextUtils.isEmpty(mEditTextEmail.getText().toString())) {
                        mEditTextEmail.setError(mActivity.getString(R.string.email_empty));
                        return;
                    }

                    if (!Utility.isEmailValid(mEditTextEmail.getText().toString())) {
                        mEditTextEmail.setError(mActivity.getString(R.string.email_invalid));
                        return;
                    }

                    Utility.hideKeypad(mEditTextEmail, mActivity);

                    if (TextUtils.isEmpty(mEditTextPassWord.getText().toString())) {
                        mEditTextPassWord.setError(mActivity.getString(R.string.password_empty));
                        return;
                    }

                    if (mEditTextPassWord.getText().toString().length() < 5) {
                        mEditTextPassWord.setError(mActivity.getString(R.string.password_valid));
                        return;
                    }

                    Utility.hideKeypad(mEditTextPassWord, mActivity);

                    if (TextUtils.isEmpty(mEditTextConfirmPassWord.getText().toString())) {
                        mEditTextConfirmPassWord.setError(mActivity.getString(R.string.registration_confirm_pwd_empty));
                        return;
                    }

                    Utility.hideKeypad(mEditTextConfirmPassWord, mActivity);

                    if (!mEditTextPassWord.getText().toString().equals(mEditTextConfirmPassWord.getText().toString())) {
                        Toast.makeText(mActivity, R.string.registration_pwd_mismatch, Toast.LENGTH_LONG).show();
                        mEditTextConfirmPassWord.setText("");
                        return;
                    }

                    if (!Utility.get_instance(mActivity).isNetworkConnected(mActivity)) {
                        Toast.makeText(mActivity, R.string.no_internet_message, Toast.LENGTH_SHORT).show();
                        return;
                    }

                    //TODO: Here on successfull registration redirect user to home screen

                    startActivity(new Intent(mActivity, HomeActivity.class));
                    mActivity.finish();
                }
            });
            return viewGroup;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private void initViews(ViewGroup viewGroup) {
        mEditTextFullName = (CustomEditText) viewGroup.findViewById(R.id.edittext_full_name);
        mEditTextEmail = (CustomEditText) viewGroup.findViewById(R.id.edittext_email);
        mEditTextPassWord = (CustomEditText) viewGroup.findViewById(R.id.edittext_password);
        mEditTextConfirmPassWord = (CustomEditText) viewGroup.findViewById(R.id.edittext_confirm_password);
        mButtonRegister = (CustomButton) viewGroup.findViewById(R.id.button_register);
        mImageView = (ImageView) viewGroup.findViewById(R.id.image_profile);
    }

    @Override
    public void onPause() {
        super.onPause();
        Utility.hideKeypad(mEditTextFullName, mActivity);
        Utility.hideKeypad(mEditTextEmail, mActivity);
        Utility.hideKeypad(mEditTextPassWord, mActivity);
        Utility.hideKeypad(mEditTextConfirmPassWord, mActivity);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case PICK_IMAGE_ID:
                Bitmap bitmap = ImagePicker.getImageFromResult(mActivity, resultCode, data);

                if (bitmap != null)
                    mImageView.setImageBitmap(bitmap);

                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }
}
