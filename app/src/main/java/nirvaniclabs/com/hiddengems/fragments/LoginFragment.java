package nirvaniclabs.com.hiddengems.fragments;


import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import nirvaniclabs.com.hiddengems.R;
import nirvaniclabs.com.hiddengems.activities.HomeActivity;
import nirvaniclabs.com.hiddengems.activities.LoginActivity;
import nirvaniclabs.com.hiddengems.customwidgets.CustomButton;
import nirvaniclabs.com.hiddengems.customwidgets.CustomEditText;
import nirvaniclabs.com.hiddengems.utils.Utility;

/**
 * This fragment will handle the user login process.
 * on successful login user is redirect to home screen of the application.
 */
public class LoginFragment extends Fragment {

    private CustomEditText mEditTextEmail, mEditTextPassword;
    private CustomButton mButtonLogin, mButtonRegister, mButtonForgotPassword;
    public static String TAG = "LoginFragment";
    private AppCompatActivity mActivity;

    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof AppCompatActivity)
            mActivity = (AppCompatActivity) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utility.adjustSoftInput(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        try {
            ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.fragment_login, container, false);

            initViews(viewGroup);

            mButtonRegister.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mActivity != null) {
                        ((LoginActivity) mActivity).replaceFragment(new SignupFragment(), "SignUp", SignupFragment.TAG);
                    }
                }
            });

            mButtonLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (TextUtils.isEmpty(mEditTextEmail.getText().toString())) {
                        mEditTextEmail.setError(mActivity.getString(R.string.email_empty));
                        return;
                    }

                    if (!Utility.isEmailValid(mEditTextEmail.getText().toString())) {
                        mEditTextEmail.setError(mActivity.getString(R.string.email_invalid));
                        return;
                    }

                    Utility.hideKeypad(mEditTextEmail, mActivity);

                    if (TextUtils.isEmpty(mEditTextPassword.getText().toString())) {
                        mEditTextPassword.setError(mActivity.getString(R.string.password_empty));
                        return;
                    }

                    if (mEditTextPassword.getText().toString().length() < 5) {
                        mEditTextPassword.setError(mActivity.getString(R.string.password_valid));
                        return;
                    }

                    Utility.hideKeypad(mEditTextPassword, mActivity);

                    if (!Utility.get_instance(mActivity).isNetworkConnected(mActivity)) {
                        Toast.makeText(mActivity, R.string.no_internet_message, Toast.LENGTH_SHORT).show();
                        return;
                    }

                    // send credentials to server and check for valid credentials

                    startActivity(new Intent(mActivity, HomeActivity.class));
                    mActivity.finish();
                }
            });

            mButtonForgotPassword.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mActivity != null) {
                        ((LoginActivity) mActivity).replaceFragment(new ForgotPasswordFragment(), mActivity.getString(R.string.forgot_pwd_header), ForgotPasswordFragment.TAG);
                    }
                }
            });

            return viewGroup;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    private void initViews(ViewGroup viewGroup) {

        mEditTextEmail = (CustomEditText) viewGroup.findViewById(R.id.edittext_email);
        mEditTextPassword = (CustomEditText) viewGroup.findViewById(R.id.edittext_password);
        mButtonLogin = (CustomButton) viewGroup.findViewById(R.id.button_login);
        mButtonRegister = (CustomButton) viewGroup.findViewById(R.id.button_signup);
        mButtonForgotPassword = (CustomButton) viewGroup.findViewById(R.id.butuon_forgot_pwd);
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            if (mActivity != null) {
                ActionBar actionBar = mActivity.getSupportActionBar();
                if (actionBar != null) {
                    actionBar.setDisplayShowTitleEnabled(false);
                    actionBar.setDisplayHomeAsUpEnabled(false);
                    actionBar.setBackgroundDrawable(new ColorDrawable(0xFFFFFFFF));
                    ((LoginActivity) mActivity).setActionBarTitle("");
                }
            }

            mEditTextEmail.setText("");
            mEditTextPassword.setText("");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
