package nirvaniclabs.com.hiddengems.fragments;


import android.content.Context;
import android.content.DialogInterface;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;

import nirvaniclabs.com.hiddengems.R;
import nirvaniclabs.com.hiddengems.activities.HomeActivity;
import nirvaniclabs.com.hiddengems.adapters.PlaceAutocompleteAdapter;
import nirvaniclabs.com.hiddengems.customwidgets.CustomAutoCompleteTextView;
import nirvaniclabs.com.hiddengems.customwidgets.CustomButton;
import nirvaniclabs.com.hiddengems.utils.Constants;
import nirvaniclabs.com.hiddengems.utils.HiddenGemsApplication;
import nirvaniclabs.com.hiddengems.utils.Utility;

/**
 * This fragment is used to accept destination of a trip from user.
 * Destination can be taken from Map or through autocomplete view.
 * This will be used in explore mode.
 */
public class PickupDestinationFragment extends Fragment implements GoogleApiClient.OnConnectionFailedListener {

    private AppCompatActivity mActivity;
    public static String TAG = "PickupDestinationFragment";

    private MapView mMapView;
    private GoogleMap mMap;
    private CustomButton mButtonStartTrip;
    private ImageButton mImageButtonStartTrip;
    private AlertDialog mAlertDialog;
    private HiddenGemsApplication mHiddenGemsApplication;
    private CustomAutoCompleteTextView mCustomAutoCompleteTextView;

    private GoogleApiClient mGoogleApiClient;
    private PlaceAutocompleteAdapter mAdapter;

    private LatLng mLatLngDestination;
    private String mDestinationName;

    // This variable is used to save map positions during life cycle of this fragment
    private CameraPosition mCameraPosition;

    public PickupDestinationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof AppCompatActivity)
            mActivity = (AppCompatActivity) context;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        if (mActivity != null)
            mHiddenGemsApplication = (HiddenGemsApplication) mActivity.getApplication();
        else {
            mActivity = (AppCompatActivity) getActivity();
            mHiddenGemsApplication = (HiddenGemsApplication) mActivity.getApplication();
        }

        super.onCreate(savedInstanceState);

        try {
            Utility.adjustSoftInput(mActivity);

            ActionBar actionBar = mActivity.getSupportActionBar();
            if (actionBar != null) {
                actionBar.setDisplayShowTitleEnabled(false);
                actionBar.setDisplayHomeAsUpEnabled(true);
            }

//            if (Utility.get_instance(mActivity).isNetworkConnected(mActivity)) {
//                AlertDialog.Builder builder
//                        = new AlertDialog.Builder(mActivity);
//
//
//                builder.setTitle(mActivity.getString(R.string.app_name))
//                        .setMessage(mActivity.getString(R.string.pick_destination_or_not));
//
//                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        mAlertDialog.dismiss();
//                    }
//                });
//
//                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        mAlertDialog.dismiss();
//                        TripFragment tripFragment = new TripFragment();
//                        Bundle bundle = new Bundle();
//                        bundle.putString(Constants.ARGUMENT_ACTIONBAR_TITLE, "Free Roam");
//                        tripFragment.setArguments(bundle);
//                        Constants.mIsExploreMode = false;
//                        Constants.mIsFreeRoamMode = true;
//                        Constants.mIsNormalTrip = false;
//                        ((HomeActivity) mActivity).replaceFragment(tripFragment, "Free Roam", TripFragment.TAG);
//                    }
//                });
//
//                mAlertDialog = builder.create();
//                mAlertDialog.setCancelable(false);
//                mAlertDialog.show();
//            }

            mGoogleApiClient = new GoogleApiClient.Builder(mActivity)
                    .addApi(Places.GEO_DATA_API)
                    .addOnConnectionFailedListener(this)
                    .build();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        try {

            if (mActivity == null)
                mActivity = (AppCompatActivity) getActivity();

            if (Utility.get_instance(mActivity).googleServiceIsOK(mActivity)) {
                ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.fragment_pickup_destination, container, false);
                initViews(viewGroup);

                if (mMapView != null)
                    mMapView.onCreate(savedInstanceState);

                checkIfMapLoaded();

                mButtonStartTrip.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startTheTrip(mCustomAutoCompleteTextView.getText().toString());
                    }
                });

                mImageButtonStartTrip.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startTheTrip(mCustomAutoCompleteTextView.getText().toString());
                    }
                });

                if (initMap()) {
                    // Shivaji Nagar is the center of the map
                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(Constants.CITY_POST_PUNE_LAT, Constants.CITY_POST_PUNE_LNG), 14.0f);
                    mMap.moveCamera(cameraUpdate);

                    mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                        @Override
                        public boolean onMarkerClick(Marker marker) {
                            return true;
                        }
                    });

                    mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                        @Override
                        public void onMapClick(LatLng latLng) {

                            if (!Utility.get_instance(mActivity).isNetworkConnected(mActivity)) {
                                Toast.makeText(mActivity, mActivity.getString(R.string.no_internet_message), Toast.LENGTH_LONG).show();
                                return;
                            }

                            String address = convertDestinationToAddress(latLng);

                            if (!TextUtils.isEmpty(address)) {
                                mMap.clear();

                                MarkerOptions markerOptions = new MarkerOptions();
                                markerOptions.position(latLng);
                                mMap.addMarker(markerOptions);
                                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

                                mCustomAutoCompleteTextView.setText(address);
                                mLatLngDestination = latLng;
                                mDestinationName = address;
                            } else
                                Toast.makeText(mActivity, R.string.error_convert_to_physical_address, Toast.LENGTH_LONG).show();
                        }
                    });

                    // Register a listener that receives callbacks when a suggestion has been selected
                    mCustomAutoCompleteTextView.setOnItemClickListener(mAutocompleteClickListener);
                    // Set up the adapter that will retrieve suggestions from the Places Geo Data API that cover
                    // the entire world.
                    mAdapter = new PlaceAutocompleteAdapter(mActivity, mGoogleApiClient, Constants.BOUNDS_PUNE_CITY,
                            null);
                    mCustomAutoCompleteTextView.setAdapter(mAdapter);
                } else
                    Toast.makeText(mActivity, "Map is not available.", Toast.LENGTH_LONG).show();

                return viewGroup;
            } else
                return inflater.inflate(R.layout.no_google_play_service_message, container, false);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            ((HomeActivity) mActivity).setDrawerIndicatorEnabled(false);
            ((HomeActivity) mActivity).enableDisableDrawer(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            ((HomeActivity) mActivity).setActionBarTitle(mActivity.getString(R.string.explore));

            initMap();

            if (mMapView != null)
                mMapView.onResume();

            if (mCameraPosition != null) {
                mMap.moveCamera(CameraUpdateFactory.newCameraPosition(mCameraPosition));
                mCameraPosition = null;
            }

            if (mLatLngDestination != null && !TextUtils.isEmpty(mDestinationName)) {
                mMap.addMarker(new MarkerOptions().title(mDestinationName).position(mLatLngDestination));
                mMap.animateCamera(CameraUpdateFactory.newLatLng(mLatLngDestination));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This API checks if selected location is in the boundary of city or not
     *
     * @return true if distance is in city boundaries
     */
    private boolean checkLocationIsInCityBoundary() {
        if (mLatLngDestination != null) {

            float distanceInKiloMeters = Utility.get_instance(mActivity).distanceBetween(new LatLng(Constants.CITY_POST_PUNE_LAT, Constants.CITY_POST_PUNE_LNG), mLatLngDestination);

            if (distanceInKiloMeters > Constants.DEFAULT_RADIUS_PUNE)
                return false;
        }
        return true;
    }

    /**
     * This redirects user to Trip Screen if user has provided the Destination.
     */
    private void startTheTrip(String place) {

        // check if  user has selected his current location or selected location from autocomplete view

        if (!Utility.get_instance(mActivity).isNetworkConnected(mActivity)) {
            Toast.makeText(mActivity, mActivity.getString(R.string.no_internet_message), Toast.LENGTH_LONG).show();
            return;
        }

        if (TextUtils.isEmpty(mCustomAutoCompleteTextView.getText().toString())) {
            Toast.makeText(mActivity, R.string.destination_place_invalid, Toast.LENGTH_LONG).show();
            return;
        }

        if (!checkLocationIsInCityBoundary()) {
            Toast.makeText(mActivity, R.string.vaild_city_boundry, Toast.LENGTH_LONG).show();
            return;
        }

        displayDestinationConfirmationDialog(place);
    }

    /**
     * Listener that handles selections from suggestions from the AutoCompleteTextView that
     * displays Place suggestions.
     * Gets the place id of the selected item and issues a request to the Places Geo Data API
     * to retrieve more details about the place.
     *
     * @see com.google.android.gms.location.places.GeoDataApi#getPlaceById(com.google.android.gms.common.api.GoogleApiClient,
     * String...)
     */
    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            Utility.hideKeypadForAutomCompleteView(mCustomAutoCompleteTextView, mActivity);

            /*
             Retrieve the place ID of the selected item from the Adapter.
             The adapter stores each Place suggestion in a AutocompletePrediction from which we
             read the place ID and title.
              */
            final AutocompletePrediction item = mAdapter.getItem(position);
            final String placeId = item.getPlaceId();
            final CharSequence primaryText = item.getPrimaryText(null);

            if (!TextUtils.isEmpty(primaryText))
                Log.i(TAG, "Autocomplete item selected: " + primaryText);

            /*
             Issue a request to the Places Geo Data API to retrieve a Place object with additional
             details about the place.
              */

            if (!TextUtils.isEmpty(placeId)) {
                PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                        .getPlaceById(mGoogleApiClient, placeId);
                if (placeResult != null)
                    placeResult.setResultCallback(mUpdatePlaceDetailsCallback);

                Log.i(TAG, "Called getPlaceById to get Place details for " + placeId);
            }
        }
    };

    /**
     * Callback for results from a Places Geo Data API query that shows the first place result in
     * the details view on screen.
     */
    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {

            if (places == null)
                return;

            if (!places.getStatus().isSuccess()) {
                // Request did not complete successfully
                Log.e(TAG, "Place query did not complete. Error: " + places.getStatus().toString());
                places.release();
                return;
            }
            // Get the Place object from the buffer.
            final Place place = places.get(0);

            mMap.clear();
            mLatLngDestination = place.getLatLng();
            mDestinationName = place.getName().toString();
            mMap.addMarker(new MarkerOptions().position(place.getLatLng())
                    .title(place.getName().toString()));
            mMap.animateCamera(CameraUpdateFactory.newLatLng(place.getLatLng()));

            places.release();
        }
    };


    /**
     * @param latLng whose physical address is required
     */
    private String convertDestinationToAddress(LatLng latLng) {

        try {

            Geocoder geocoder = new Geocoder(mActivity);
            List<Address> addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
            if (addresses != null && addresses.size() > 0) {
                String address = addresses.get(0).getAddressLine(0);
                String sublocality = addresses.get(0).getAddressLine(1);
                String city = addresses.get(0).getAddressLine(2);

                String place = "";

                if (!TextUtils.isEmpty(address))
                    place += address;

                if (!TextUtils.isEmpty(sublocality))
                    place = place + "," + sublocality;

                if (!TextUtils.isEmpty(city))
                    place = place + "," + city;

                if (place.length() > 0 && (place.charAt(0) == ','))
                    place = place.substring(1);

                return place;
            }
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void displayDestinationConfirmationDialog(String place) {

        Utility.hideKeypadForAutomCompleteView(mCustomAutoCompleteTextView, mActivity);

        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);

        builder.setTitle(mActivity.getString(R.string.app_name))
                .setMessage("Do you want to set: " + place + " as your destination point ?");

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {


                if (mAlertDialog != null) {
                    mAlertDialog.dismiss();
                }

                TripFragment tripFragment = new TripFragment();
                Bundle bundle = new Bundle();
                bundle.putString(Constants.ARGUMENT_ACTIONBAR_TITLE, mActivity.getString(R.string.explore));
                tripFragment.setArguments(bundle);

                HomeActivity.mLatLngDestination = mLatLngDestination;

                // TODO: uncomment this
//                if (!compareSourceAndDestinationLatLng()) {
//                    Toast.makeText(mActivity, "Distance between source and destination must be " + Constants.DEFAULT_TRIP_DISTANCE + " Km", Toast.LENGTH_LONG).show();
//                    return;
//                }

                Constants.mIsExploreMode = true;
                Constants.mIsFreeRoamMode = false;
                Constants.mIsNormalTrip = false;

                ((HomeActivity) mActivity).replaceFragment(tripFragment, mActivity.getString(R.string.explore), TripFragment.TAG);
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (mAlertDialog != null) {
                    mAlertDialog.dismiss();
                }
            }
        });

        mAlertDialog = builder.create();
        mAlertDialog.setCancelable(false);
        mAlertDialog.show();
    }

    private void initViews(ViewGroup viewGroup) {
        mMapView = (MapView) viewGroup.findViewById(R.id.mapview);
        mButtonStartTrip = (CustomButton) viewGroup.findViewById(R.id.button_done);
        mImageButtonStartTrip = (ImageButton) viewGroup.findViewById(R.id.imagebutton_arrow);
        mCustomAutoCompleteTextView = (CustomAutoCompleteTextView) viewGroup.findViewById(R.id.edittext_enter_dest);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mMapView != null)
            mMapView.onPause();

        if (mActivity != null)
            Utility.hideKeypadForAutomCompleteView(mCustomAutoCompleteTextView, mActivity);

        mCameraPosition = mMap.getCameraPosition();
        mMap = null;
    }

    @Override
    public void onStop() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected())
            mGoogleApiClient.disconnect();
        ((HomeActivity) mActivity).enableDisableDrawer(DrawerLayout.LOCK_MODE_UNLOCKED);
        super.onStop();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if (mMapView != null)
            mMapView.onLowMemory();
    }

    @Override
    public void onDestroy() {
        if (mMapView != null)
            mMapView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        mActivity = null;
        super.onDestroyView();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mMapView != null)
            mMapView.onSaveInstanceState(outState);
    }

    /**
     * This method checks if map is available or not
     *
     * @return true if map available else false
     */
    private boolean initMap() {

        if (mMapView == null)
            return false;

        if (mMap == null) {
            mMap = mMapView.getMap();
        }
        return (mMap != null);
    }

    private void displayAlert() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(mActivity);

        builder.setMessage(R.string.no_internet_while_mapload)
                .setTitle(getResources().getString(R.string.app_name));

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked OK button
                dialog.dismiss();
            }
        });

        android.app.AlertDialog dialog = builder.create();

        dialog.show();
    }

    /**
     * This method checks if map is loaded when user comes to this screen for the first time
     */
    private void checkIfMapLoaded() {
        try {

            if (mHiddenGemsApplication.mGlobalSharedPreferences.getBoolean(Constants.PREFS_IS_MAP_LOADED_ON_DEST_SCREEN, false))
                return;

            final int DOESNOT_EXIST = -1;

            int currentVersionCode;
            try {
                currentVersionCode = mActivity.getPackageManager().getPackageInfo(mActivity.getPackageName(), 0).versionCode;
            } catch (android.content.pm.PackageManager.NameNotFoundException e) {
                e.printStackTrace();
                return;
            }

            int savedVersionCode = mHiddenGemsApplication.mGlobalSharedPreferences.getInt(Constants.PREF_VERSION_CODE_KEY, DOESNOT_EXIST);


            // Check for first run or upgrade
            if (currentVersionCode == savedVersionCode) {

                // This is just a normal run
                if (!mHiddenGemsApplication.mGlobalSharedPreferences.getBoolean(Constants.PREFS_IS_MAP_LOADED_ON_DEST_SCREEN, false) && (!Utility.get_instance(mActivity).isNetworkConnected(mActivity))) {
                    displayAlert();
                } else
                    mHiddenGemsApplication.mGlobalSharedPreferences.edit().putBoolean(Constants.PREFS_IS_MAP_LOADED_ON_DEST_SCREEN, true).apply();
                return;

            } else if (savedVersionCode == DOESNOT_EXIST) {

                // This is a new install (or the user cleared the shared preferences)

                // Check for network connection and if no network then display alert

                if (!Utility.get_instance(mActivity).isNetworkConnected(mActivity)) {

                    displayAlert();

                    mHiddenGemsApplication.mGlobalSharedPreferences.edit().putBoolean(Constants.PREFS_IS_MAP_LOADED_ON_DEST_SCREEN, false).apply();

                } else
                    mHiddenGemsApplication.mGlobalSharedPreferences.edit().putBoolean(Constants.PREFS_IS_MAP_LOADED_ON_DEST_SCREEN, true).apply();
            }

        /*else if (currentVersionCode > savedVersionCode) {

              This is an upgrade

        }*/

            // Update the shared preferences with the current version code
            mHiddenGemsApplication.mGlobalSharedPreferences.edit().putInt(Constants.PREF_VERSION_CODE_KEY, currentVersionCode).apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    private boolean compareSourceAndDestinationLatLng() {

        float distanceInKiloMeters = Utility.get_instance(mActivity).distanceBetween(HomeActivity.mLatLngSource, HomeActivity.mLatLngDestination);

        return distanceInKiloMeters >= Constants.DEFAULT_TRIP_DISTANCE;
    }
}
