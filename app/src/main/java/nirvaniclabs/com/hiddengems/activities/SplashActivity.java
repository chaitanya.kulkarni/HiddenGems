package nirvaniclabs.com.hiddengems.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;

import nirvaniclabs.com.hiddengems.R;
import nirvaniclabs.com.hiddengems.utils.HiddenGemsApplication;
import nirvaniclabs.com.hiddengems.utils.Utility;

/**
 * Created by Amrut
 * Launch Screen of the application.
 */
public class SplashActivity extends AppCompatActivity {

    private static final int MSG_CONTINUE = 2000;
    private static final int DELAY = 2000;
    private HiddenGemsApplication mHiddenGemsApplication;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            getWindow().requestFeature(Window.FEATURE_NO_TITLE);

            if (Utility.isLargeScreen(this))
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            else
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

            mHiddenGemsApplication = (HiddenGemsApplication) getApplication();

            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_splash);
            mHandler.sendEmptyMessageDelayed(MSG_CONTINUE, DELAY);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @SuppressLint("HandlerLeak")
    private final Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case MSG_CONTINUE:
                    startHomeActivity();
                    break;
            }
        }
    };

    private void startHomeActivity() {

//        String mUserEmail = mHiddenGemsApplication.mGlobalSharedPreferences.getString(Constants.PREF_USER_EMAIL, null);
//        if (TextUtils.isEmpty(mUserEmail))
//            startActivity(new Intent(this, LoginActivity.class));
//        else
        startActivity(new Intent(this, HomeActivity.class));

        finish();
    }

    @Override
    protected void onDestroy() {
        mHandler.removeMessages(MSG_CONTINUE);
        Utility.makeInstanceNull();
        super.onDestroy();
    }

}
